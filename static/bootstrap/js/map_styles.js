/**
 * Created by pepijnmulders on 9/23/15.
 */
var map_styles = [
    {
        "elementType": "labels",
        "stylers": [{"visibility": "off"}]
    },
    {
        "elementType": "geometry",
        "stylers": [{"visibility": "off"}]
    }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [{"visibility": "on"}, {"color": "#000000"}]
    }, {
        "featureType": "landscape",
        "stylers": [{"color": "#ffffff"},
        {"visibility": "on"}]
    }
]