from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Accounts urls if logged in accounts redirects
    url(r'', include('accounts.urls')),

    # API URLS to display forms in general
    url(r'^api/', include('api.urls')),

    # Estate URLS
    url(r'^estate/', include('estate.urls')),

    # Site-packages URLS
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
]
