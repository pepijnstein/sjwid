from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.core.serializers import serialize
from utils.postcodenl import PostcodeNL
from .models import *
from .forms import *
import json


# Create your views here.
@login_required()
def estate_dashboard(request):
    context = RequestContext(request)
    return render_to_response('estate/pages/estate_dashboard.html', {}, context)


@login_required()
def object_add(request):
    context = RequestContext(request)
    agents = Agent.objects.all().order_by('id')
    return render_to_response('estate/pages/object_add.html', {'agents': agents}, context)


@login_required()
def object_overview(request):
    context = RequestContext(request)
    objects = Object.objects.all()
    return render_to_response('estate/pages/estate_objects_overview.html', {'objects': objects}, context)


@login_required()
def agent_add(request):
    context = RequestContext(request)
    if request.POST:
        form = AddAgentForm(request.POST)
        if form.is_valid():
            agent = Agent()

            agent.name = request.POST.get('name', False)
            agent.first_name = request.POST.get('first_name', False)
            agent.last_name = request.POST.get('last_name', False)
            agent.street = request.POST.get('street', False)
            agent.nr = request.POST.get('nr', False)
            agent.nr_prefix = request.POST.get('nr_prefix', False)
            agent.zip = request.POST.get('zip', False)
            agent.town = request.POST.get('town', False)
            agent.email = request.POST.get('email', False)
            agent.phone = request.POST.get('phone', False)

            agent.save()

        else:
            print 'form invalid!'
        return render_to_response('estate/pages/agent_add.html', {'form': form, 'button_text': 'Makelaar toevoegen'}, context)
    else:
        form = AddAgentForm()
        return render_to_response('estate/pages/agent_add.html', {'form': form, 'button_text': 'Makelaar toevoegen'}, context)


@login_required()
def agent_overview(request):
    context = RequestContext(request)
    agents = Agent.objects.all()
    return render_to_response('estate/pages/agent_overview.html', {'agents': agents}, context)


# @csrf_exempt
# def agent_info(request):
#     if request.POST:
#         data = json.loads(request.body)
#         id = str(data['id'])
#         agent = Agent.objects.filter(id=id)
#         return JsonResponse({'data': serialize('python', agent)})
#     else:
#         return JsonResponse({
#             'status': 403,
#             'message': 'no post info received.'
#         })