=========
kpi_infra
=========

Utilities is a simple Django app to conduct Web-based utilities.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "utilities" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'utilities',
    )

2. Include the utilities URLconf in your project urls.py like this::

    url(r'^utilities/', include('utilities.urls')),

3. Run `python manage.py migrate` to create the utilities models.

4. Visit http://127.0.0.1:8000/utilities/ to check functionality.