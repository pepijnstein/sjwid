from django.contrib.gis.db import models as geo_models
from sjwid.settings import BASE_DIR
from django.db import models
import os


# Create your models here.
PRICE_TYPE_LIST = (
    ('RM', 'RENT_PER_MONTH'),
    ('RY', "RENT_PER_YEAR"),
    ('FS', 'FOR_SALE'),
)


class Agent(models.Model):
    name = models.CharField(max_length=255, default=None)
    first_name = models.CharField(max_length=100, default=None, null=True, blank=True)
    last_name = models.CharField(max_length=100, default=None, null=True, blank=True)
    street = models.CharField(max_length=255, default=None)
    nr = models.IntegerField(default=None)
    nr_prefix = models.CharField(max_length=11, default=None, null=True, blank=True)
    zip = models.CharField(max_length=6, default=None)
    town = models.CharField(max_length=200, default=None)
    email = models.EmailField(default=None)
    phone = models.CharField(max_length=12, default=None)
    image = models.ImageField(default=None, null=True, upload_to=os.path.join(BASE_DIR, 'estate/static/estate/uploads/images/agents'), max_length=99999)
    geom = geo_models.PointField(srid=4326, default=None, null=True)

    def __str__(self):
        return self.name


class Object(models.Model):
    agent = models.ForeignKey(Agent, default=None)
    street = models.CharField(max_length=255, default=None)
    nr = models.IntegerField(default=None)
    nr_prefix = models.CharField(max_length=11, default=None, null=True, blank=True)
    zip = models.CharField(max_length=6, default=None)
    town = models.CharField(max_length=200, default=None)
    price_type = models.CharField(max_length=100, choices=PRICE_TYPE_LIST)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(default=None, null=True, blank=True, upload_to=os.path.join(BASE_DIR, 'estate/static/estate/uploads/images/objects'), max_length=99999)
    url = models.URLField(default=None, null=True, blank=True)
    geom = geo_models.PointField(srid=4326, default=None)

    # def save(self, *args, **kwargs):
    #     print 'Object model saved!'