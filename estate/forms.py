from django import forms


# Create your forms here
class ObjectInfoForm(forms.Form):
    zip = forms.CharField(label='Postcode')
    nr = forms.CharField(label='Huisnummer')


PRICE_TYPE_LIST = (
    ('RM', 'RENT_PER_MONTH'),
    ('RY', "RENT_PER_YEAR"),
    ('FS', 'FOR_SALE'),
)


class AddObjectForm(forms.Form):
    # Todo: add required fields
    agent = forms.IntegerField(label='Agent id', required=True)
    street = forms.CharField(max_length=255, label='Street', required=True)
    nr = forms.IntegerField(label='house number', required=True)
    nr_prefix = forms.CharField(max_length=11, label='house number prefix')
    zip = forms.CharField(max_length=6, label='Zipcode', required=True)
    town = forms.CharField(max_length=255, label='Town', required=True)
    price_type = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=PRICE_TYPE_LIST, required=True)
    price = forms.DecimalField(label='Price', required=True)
    image = forms.ImageField(label='Image', required=True)
    url = forms.URLField(label='URL, required=True')
    latitude = forms.FloatField(label='Latitude', required=True)
    longitude = forms.FloatField(label='Longitude', required=True)


class AddAgentForm(forms.Form):
    name = forms.CharField(max_length=255, label='Makelaar Bedrijfsnaam', required=True)
    first_name = forms.CharField(max_length=255, label='Voornaam contactpersoons', required=True)
    last_name = forms.CharField(max_length=255, label='Achternaam contactpersoons', required=True)
    street = forms.CharField(max_length=255, label='Straat', required=True)
    nr = forms.IntegerField(label='Huisnummer', required=True)
    nr_prefix = forms.CharField(max_length=11, label='Toevoeging', required=False)
    zip = forms.CharField(max_length=6, label='Postcode', required=True)
    town = forms.CharField(max_length=255, label='Plaats', required=True)
    email = forms.EmailField(label='Email', required=True)
    phone = forms.CharField(max_length=12, label='Telefoonnummer')