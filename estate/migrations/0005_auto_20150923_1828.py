# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0004_agent_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='object',
            name='nr_prefix',
            field=models.CharField(default=None, max_length=11, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='object',
            name='price',
            field=models.DecimalField(max_digits=10, decimal_places=2),
        ),
    ]
