# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0002_remove_agent_geom'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='geom',
            field=django.contrib.gis.db.models.fields.PointField(default=None, srid=4326, null=True),
        ),
    ]
