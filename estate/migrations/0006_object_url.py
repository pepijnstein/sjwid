# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0005_auto_20150923_1828'),
    ]

    operations = [
        migrations.AddField(
            model_name='object',
            name='url',
            field=models.URLField(default=None, null=True, blank=True),
        ),
    ]
