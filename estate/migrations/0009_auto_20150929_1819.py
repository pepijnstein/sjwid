# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0008_auto_20150929_1818'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agent',
            name='image',
            field=models.ImageField(default=None, max_length=99999, null=True, upload_to=b'/Users/pepijnmulders/Applications/Python/sjwid/estate/static/estate/uploads/images/agents'),
        ),
        migrations.AlterField(
            model_name='object',
            name='image',
            field=models.ImageField(default=None, max_length=99999, upload_to=b'/Users/pepijnmulders/Applications/Python/sjwid/estate/static/estate/uploads/images/objects'),
        ),
    ]
