# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0006_object_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agent',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b'/Users/pepijnmulders/Applications/Python/sjwid/estate/static/etate/uploads/images/agents'),
        ),
        migrations.AlterField(
            model_name='object',
            name='image',
            field=models.ImageField(default=None, upload_to=b'/Users/pepijnmulders/Applications/Python/sjwid/estate/static/etate/uploads/images/objects'),
        ),
    ]
