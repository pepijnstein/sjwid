# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('estate', '0003_agent_geom'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='image',
            field=models.ImageField(default=None, null=True, upload_to=b''),
        ),
    ]
