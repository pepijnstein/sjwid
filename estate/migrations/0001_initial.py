# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Agent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=None, max_length=255)),
                ('first_name', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('last_name', models.CharField(default=None, max_length=100, null=True, blank=True)),
                ('street', models.CharField(default=None, max_length=255)),
                ('nr', models.IntegerField(default=None)),
                ('nr_prefix', models.CharField(default=None, max_length=11, null=True, blank=True)),
                ('zip', models.CharField(default=None, max_length=6)),
                ('town', models.CharField(default=None, max_length=200)),
                ('email', models.EmailField(default=None, max_length=254)),
                ('phone', models.CharField(default=None, max_length=12)),
                ('geom', django.contrib.gis.db.models.fields.PointField(default=None, srid=4326, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Object',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(default=None, max_length=255)),
                ('nr', models.IntegerField(default=None)),
                ('nr_prefix', models.CharField(default=None, max_length=11)),
                ('zip', models.CharField(default=None, max_length=6)),
                ('town', models.CharField(default=None, max_length=200)),
                ('price_type', models.CharField(max_length=100, choices=[(b'RM', b'RENT_PER_MONTH'), (b'RY', b'RENT_PER_YEAR'), (b'FS', b'FOR_SALE')])),
                ('price', models.DecimalField(max_digits=6, decimal_places=2)),
                ('image', models.ImageField(default=None, upload_to=b'')),
                ('geom', django.contrib.gis.db.models.fields.PointField(default=None, srid=4326)),
                ('agent', models.ForeignKey(default=None, to='estate.Agent')),
            ],
        ),
    ]
