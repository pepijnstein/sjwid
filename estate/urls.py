from django.conf.urls import include, url
from .views import *


urlpatterns = [
    # Objects URLS
    url(r'^objects/add/', view=object_add, name='Toevoegen van estate objecten'),
    url(r'^objects/overview/', view=object_overview, name='Overview van estate objecten'),
    # Todo: add object overview
    url(r'^$', view=estate_dashboard, name='Object overview View'),

    # Agent URLS
    url(r'^agents/add/', view=agent_add, name='Get json response from agent_id'),
    url(r'^agents/overview/', view=agent_overview, name='Get json response from agent_id'),

    # Todo: Check if deprecated
    # url(r'^agent_info/', view=agent_info, name='Get json response from agent_id'),
]
