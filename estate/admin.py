from django.contrib import admin
from .models import *


class AgentAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'email',
        'phone'
    ]
    search_fields = [
        'name',
        'street'
    ]


class ObjectAdmin(admin.ModelAdmin):
    list_display = [
        'street',
        'nr',
        'nr_prefix',
        'zip',
    ]
    search_fields = [
        'agent'
    ]


# Register your models here.
admin.site.register(Agent, AgentAdmin)
admin.site.register(Object, ObjectAdmin)