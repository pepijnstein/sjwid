from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth import authenticate, login, logout
from .forms import UserLoginForm


# Create your views here.
def user_login(request):
    if request.POST:
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/estate/')
                else:
                    print 'User is blocked'
            else:
                print 'no valid user!'
        else:
            print 'form not valid!'
    else:
        if request.user.id is not None:
            return HttpResponseRedirect('/estate/')
        else:
            context = RequestContext(request)
            return render_to_response('accounts/modules/form.html', {
                'form': UserLoginForm(),
                'button_text': 'Login'
            }, context)


def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')