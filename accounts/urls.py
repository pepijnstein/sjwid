from django.conf.urls import include, url
from .views import *


urlpatterns = [
    url(r'^$', view=user_login, name='User login view'),
    url(r'^logout/', view=user_logout, name='User logout view'),
]
