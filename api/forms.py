from django import forms


# Create your forms here
class ObjectInfoForm(forms.Form):
    zip = forms.CharField(label='Postcode')
    nr = forms.CharField(label='Huisnummer')


class AgentInfoForm(forms.Form):
    agent_id = forms.CharField(label='Agent id')


PRICE_TYPE_LIST = (
    ('RM', 'RENT_PER_MONTH'),
    ('RY', "RENT_PER_YEAR"),
    ('FS', 'FOR_SALE'),
)


class AddObjectForm(forms.Form):
    # Todo: add required fields
    agent = forms.IntegerField(label='Agent id', required=True)
    street = forms.CharField(max_length=255, label='Street', required=True)
    nr = forms.IntegerField(label='house number', required=True)
    nr_prefix = forms.CharField(max_length=11, label='house number prefix')
    zip = forms.CharField(max_length=6, label='Zipcode', required=True)
    town = forms.CharField(max_length=255, label='Town', required=True)
    price_type = forms.ChoiceField(choices=PRICE_TYPE_LIST, required=True)
    price = forms.DecimalField(label='Price', required=True)
    image = forms.ImageField(label='Image', required=True)
    url = forms.URLField(label='URL', required=True)
    latitude = forms.FloatField(label='Latitude', required=True)
    longitude = forms.FloatField(label='Longitude', required=True)


class DeleteObjectForm(forms.Form):
    object_id = forms.IntegerField(label='Object id', required=True)

class DeleteAgentForm(forms.Form):
    agent_id = forms.IntegerField(label='Agent id', required=True)