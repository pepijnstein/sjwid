from django.shortcuts import render_to_response, RequestContext
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from django.core.serializers import serialize
from django.contrib.gis.geos import Point
from utils.postcodenl import PostcodeNL
from estate.models import *
from estate.forms import *
from .forms import *


# Create your views here.
@login_required()
@csrf_exempt
def object_add_json(request):
    if request.POST:
        try:
            agent = Agent.objects.get(id=request.POST.get('agent_id'))
            geom = Point(float(request.POST.get('lon', False)), float(request.POST.get('lat', False)))

            model = Object()

            model.agent = agent
            model.street = request.POST.get('street', False)
            model.nr = request.POST.get('nr', False)
            model.nr_prefix = request.POST.get('nr_prefix', False)
            model.zip = request.POST.get('zip', False)
            model.town = request.POST.get('town', False)
            model.price_type = request.POST.get('price_type', False)
            model.price = request.POST.get('price', False)
            model.image = request.POST.get('image', False)
            model.url = request.POST.get('url', False)
            model.geom = geom

            model.save()
        except Exception, e:
            print e

        return JsonResponse({
            'status': 200,
            'success': True,
            'message': 'OK',
            'data': []
        })
    else:
        context = RequestContext(request)
        return render_to_response('api/modules/form.html', {
            'form': AddObjectForm(),
            'button_text': 'Add object'
        }, context)


@login_required()
@csrf_exempt
def object_delete_json(request):
    if request.POST:
        object_id = request.POST.get('object_id')
        try:
            Object.objects.get(id=object_id).delete()
            return JsonResponse({
                'status': 200,
                'success': True,
                'message': 'Het object is succesvol verwijderd!',
                'data': []
            }, status=200)
        except ObjectDoesNotExist:
            return JsonResponse({
                'status': 404,
                'success': False,
                'message': 'Dit object bestaat niet (meer)!',
                'data': []
            }, status=404)
    else:
        context = RequestContext(request)
        return render_to_response('api/modules/form.html', {
            'form': DeleteObjectForm(),
            'button_text': 'Delete object'
        }, context)


@login_required()
@csrf_exempt
def agent_add_json(request):
    if request.POST:
        return 'Blaat'
    else:
        context = RequestContext(request)
        return render_to_response('api/modules/form.html', {
            'form': AddAgentForm(),
            'button_text': 'Add estate agent'
        }, context)


@login_required()
@csrf_exempt
def agent_delete_json(request):
    if request.POST:
        agent_id = request.POST.get('agent_id')
        try:
            Agent.objects.get(id=agent_id).delete()
            return JsonResponse({
                'status': 200,
                'success': True,
                'message': 'De makelaar is succesvol verwijderd!',
                'data': []
            }, status=200)
        except ObjectDoesNotExist:
            return JsonResponse({
                'status': 404,
                'success': False,
                'message': 'Deze makelaar bestaat niet (meer)!',
                'data': []
            }, status=404)
    else:
        context = RequestContext(request)
        return render_to_response('api/modules/form.html', {
            'form': DeleteAgentForm(),
            'button_text': 'verwijder Makelaar'
        }, context)


@login_required()
@csrf_exempt
def location_info_json(request):
    if request.POST:
        zip = request.POST.get('zip')
        nr = request.POST.get('nr')

        json_data = PostcodeNL(
            zip=zip,
            nr=nr,
        )
        json_data = json_data.json()
        return JsonResponse(json_data)
    else:
        context = RequestContext(request)
        form = ObjectInfoForm()
        return render_to_response('api/modules/form.html', {
            'form': form,
            'button_text': 'Object info'
        })


@login_required()
@csrf_exempt
def agent_info_json(request):
    if request.POST:
        agent_id = request.POST.get('agent_id')
        agent = Agent.objects.filter(id=agent_id)
        agent = serialize('python', agent)

        return JsonResponse({
            'status': 200,
            'message': 'OK',
            'data': agent[0]
        })
    else:
        context = RequestContext(request)
        return render_to_response('api/modules/form.html', {
            'form': AgentInfoForm(),
            'button_text': 'Object info'
        })


def map_example(request):
    context = RequestContext(request)
    return render_to_response('api/pages/map_example.html')


def map_feed_json(request):
    objects = Object.objects.all()
    return HttpResponse(serialize('geojson', objects), content_type='Application/json')
