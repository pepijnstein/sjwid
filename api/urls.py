from django.conf.urls import include, url
from .views import *


urlpatterns = [

    url(r'^$', view=map_example, name='Map Example'),

    # Form to add the object after all steps
    url(r'^object_add.json', view=object_add_json, name='Form to add object'),
    url(r'^object_delete.json', view=object_delete_json, name='Form to add object'),

    # Step 1 location info
    url(r'^location_info.json', view=location_info_json, name='Form to return location information'),

    # Step 2 Agent info
    url(r'^agent_info.json', view=agent_info_json, name='Form to return location information'),
    url(r'^agent_add.json', view=agent_add_json, name='Maak nieuwe makelaar aan'),
    url(r'^agent_delete.json', view=agent_delete_json, name='Delete agents from json'),

    # Geojson result of all objects
    url(r'^map_feed.json', view=map_feed_json, name='View to return JsonResponse of all objects')
]
