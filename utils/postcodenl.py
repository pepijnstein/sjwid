import urllib2
import json


class PostcodeNL:
    def __init__(self, zip=None, nr=None):
        self.api_key = '7e49a3464c6ffb8f4818b1467fb387deac3e14c5'
        self.zip = zip
        self.nr = nr

    def json(self):
        url = 'http://api.postcodeapi.nu/'+self.zip+'/'+self.nr
        headers = {
            'Api-Key': self.api_key
        }
        request = urllib2.Request(url, headers=headers)
        response = urllib2.urlopen(request)
        json_response = json.loads(response.read())
        return json_response

    def xml(self):
        print 'XML output for later purpose'
        return self.api_key



# zip = PostcodeNL(
#     zip='3512bx',
#     nr='17'
# )
# json = zip.json()
# print json['resource']['town']
# print json['resource']['street']
